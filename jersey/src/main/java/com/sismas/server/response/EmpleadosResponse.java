package com.sismas.server.response;

import com.sismas.dominio.Empleado;
import java.util.List;

public class EmpleadosResponse {

    private List<Empleado> empleados;

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

}
