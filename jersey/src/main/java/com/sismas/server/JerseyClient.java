package com.sismas.server;

import com.sismas.server.response.EmpleadosResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.jackson.JacksonFeature;

@Path("/usuarios")
public class JerseyClient {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public EmpleadosResponse getConsumeEmpleados() {
        Client client = ClientBuilder.newClient().register(JacksonFeature.class);
        EmpleadosResponse empleadosResponse = client.target("http://localhost:8080/jerseyServer/")
                .path("rest/empleados")
                .request(MediaType.APPLICATION_JSON)
                .get(EmpleadosResponse.class);
        System.out.println("empleadosResponse: " + empleadosResponse);
        return empleadosResponse;
    }
}
