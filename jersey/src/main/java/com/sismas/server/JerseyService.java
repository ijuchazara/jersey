package com.sismas.server;

import com.sismas.dominio.Empleado;
import com.sismas.server.response.EmpleadosResponse;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/empleados")
public class JerseyService {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public EmpleadosResponse getAllEmpleados() {
        EmpleadosResponse empleadosResponse = new EmpleadosResponse();
        List empleados = new ArrayList();
        empleados.add(new Empleado(1, "Lokesh Gupta"));
        empleados.add(new Empleado(2, "Alex Kolenchiskey"));
        empleados.add(new Empleado(3, "David Kameron"));
        empleadosResponse.setEmpleados(empleados);
        return empleadosResponse;
    }

}
